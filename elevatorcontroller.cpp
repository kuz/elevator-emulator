/*
 * elevator.cpp
 *
 *  Created on: Sep 10, 2017
 *      Author: kuzmichev
 */

#include <unistd.h>
#include "elevatorcontroller.h"

ElevatorController::ElevatorController(uint maxFloors)
{
	m_maxFloors = maxFloors;
}

ElevatorController::~ElevatorController()
{
}

ElevatorController::State ElevatorController::getState() const
{
    std::lock_guard<std::mutex> locker(m_mtx);
	return m_state;
}

Floor ElevatorController::getFloor() const
{
    std::lock_guard<std::mutex> locker(m_mtx);
    return m_currentFloor;
}

void ElevatorController::start()
{
    std::cout << __PRETTY_FUNCTION__ << "\n";
}

void ElevatorController::start(std::function<void(Floor, State)> func)
{
    setNotificationFunction(func);
    f_notification(m_currentFloor, m_state);
    m_moveThread = std::thread(&ElevatorController::move, this);
}

void ElevatorController::setNotificationFunction(std::function<void(Floor, ElevatorController::State)> func)
{
    f_notification = func;
}

void ElevatorController::processCommand(const std::string& command)
{
    if (command.size() < 2) {
        std::cout << "[WARNING] Incorrect command\n";
        return;
    }

    Floor floor = 0;
    if (std::toupper(command[0]) == COMMAND_TYPE_CABIN) {
        std::cout << __PRETTY_FUNCTION__ << " CABIN\n";
    } else if (std::toupper(command[0]) == COMMAND_TYPE_FLOOR) {
        std::cout << __PRETTY_FUNCTION__ << " FLOOR\n";
    } else {
        std::cout << "[WARNING] Incorrect command\n";
        return;
    }
    floor = std::stoi(command.substr(1));
    if (floor > m_maxFloors) {
        std::cout << "[WARNING] Incorrect floor\n" <<
                "Entered: " << command.substr(1) << ". Floors in the building are 1 through " <<
                m_maxFloors << ".\n";
        return;
    }

    std::lock_guard<std::mutex> locker(m_mtx);
    m_floorQueue.insert(floor);
}

void ElevatorController::move()
{
    f_notification(m_currentFloor, m_state);
    while (true) {
        if (m_state == StateMovingDown) {
            while (m_currentFloor > 0) {
                sleep(1);
                goDown();
                f_notification(m_currentFloor, m_state);
            }
        } else if (m_state == StateMovingUp) {
            while (m_currentFloor < m_maxFloors) {
                sleep(1);
                goUp();
                f_notification(m_currentFloor, m_state);
            }
        }

//        f_notification(m_currentFloor, m_state);
    }
}

void ElevatorController::setState(State state)
{
//    std::cout << __PRETTY_FUNCTION__ << " state " << state;
    std::lock_guard<std::mutex> locker(m_mtx);
	m_state = state;
}

void ElevatorController::goUp()
{
    std::lock_guard<std::mutex> locker(m_mtx);
    ++m_currentFloor;
}

void ElevatorController::goDown()
{
    std::lock_guard<std::mutex> locker(m_mtx);
    --m_currentFloor;
}
