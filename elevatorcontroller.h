/*
 * elevator.h
 *
 *  Created on: Sep 10, 2017
 *      Author: kuzmichev
 */

#ifndef ELEVATORCONTROLLER_H_
#define ELEVATORCONTROLLER_H_

#include <iostream>
#include <map>
#include <functional>
#include <thread>
#include <mutex>
#include <set>

typedef uint Floor;

class ElevatorController {
public:
	ElevatorController(uint maxFloors = 0);
	virtual ~ElevatorController();

	enum State {
		StateStoppedClosed,
		StateStoppedOpen,
		StateMovingUp,
		StateMovingDown
	};


	State getState() const;
	Floor getFloor() const;

	void start();
	void start(std::function<void(Floor, State)> func);
	void setNotificationFunction(std::function<void(Floor, State)> func);
	void processCommand(const std::string& command);
    void move();

    static const char COMMAND_TYPE_FLOOR = 'F';
    static const char COMMAND_TYPE_CABIN = 'C';

private:
    void setState(State state);
    void goUp();
    void goDown();

    Floor m_maxFloors = 0;
	State m_state = StateStoppedClosed;
	Floor m_currentFloor = 0;
	std::function<void(Floor, State)> f_notification;
	std::thread m_moveThread;
	mutable std::mutex m_mtx;
    std::set<Floor> m_floorQueue;
};

#endif /* ELEVATORCONTROLLER_H_ */
