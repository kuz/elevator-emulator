/*
 * elevatorcontroller.cpp
 *
 *  Created on: Sep 10, 2017
 *      Author: kuzmichev
 */

#include <functional>
#include <istream>
#include <unistd.h>
#include <termios.h>
#include "elevator.h"

const std::map<ElevatorController::State, std::string> Elevator::s_stateDescriptions =
		{{ElevatorController::StateStoppedClosed, "Stopped. Doors are closed"},
		        {ElevatorController::StateStoppedOpen, "Stopped. Doors opened"},
		        {ElevatorController::StateMovingDown, "Going down."},
		        {ElevatorController::StateMovingUp, "Going up"}};

Elevator::Elevator()
{
}

Elevator::~Elevator()
{
	// TODO Auto-generated destructor stub
}

void Elevator::start()
{
    std::cout << "Enter the number of floors in the building\n";

    uint maxFloors = 0;
    std::cin >> maxFloors;

    m_controller = new ElevatorController(maxFloors);

    std::function<void(Floor, ElevatorController::State)> f_printState =
            [=](Floor floor, ElevatorController::State state) {printState(floor, state);};
    m_controller->start(f_printState);

    while (true) {
        getch();
        
//        std::lock_guard<std::mutex> locker(m_mtx);
        m_mtx.lock();
        std::cout << "\nEnter a command:\n" <<
                "\t" << ElevatorController::COMMAND_TYPE_FLOOR <<
                "<floor_number> to call elevator to a specified floor\n" <<
                "\t" << ElevatorController::COMMAND_TYPE_CABIN <<
                "<floor_number> (from cabin) to go to a specified floor\n";
        std::string command;
        std::cin >> command;
        m_mtx.unlock();
        m_controller->processCommand(command);
//        std::cout << command << "\n";
    }
}

void Elevator::printState(Floor floor, ElevatorController::State state)
{
    std::lock_guard<std::mutex> locker(m_mtx);
    std::cout << "Cabin on floor " << floor << ". " << s_stateDescriptions.at(state) <<
            " <Press any key to enter new command>\n";
}

char Elevator::getch() {
    char buf = 0;
    struct termios old = {0};

    if (tcgetattr(0, &old) < 0) {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0) {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0) {
        perror ("read()");
    }
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror ("tcsetattr ~ICANON");
    }

    return buf;
}
