//============================================================================
// Name        : elevator-emulator.cpp
// Author      : Kuzmichev
//============================================================================

#include <stdio.h>
#include <stdlib.h>

#include "elevator.h"

int main(void) {
	Elevator elevator;
	elevator.start();
	return EXIT_SUCCESS;
}
