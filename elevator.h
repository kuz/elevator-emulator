/*
 * elevatorcontroller.h
 *
 *  Created on: Sep 10, 2017
 *      Author: kuzmichev
 */

#ifndef ELEVATOR_H_
#define ELEVATOR_H_

#include <map>
#include <string>
#include <mutex>
#include "elevatorcontroller.h"

class Elevator {
public:
	Elevator();
	virtual ~Elevator();

	void start();

private:
	static const std::map<ElevatorController::State, std::string> s_stateDescriptions;
	ElevatorController* m_controller = nullptr;
	std::mutex m_mtx;

	void printState(Floor floor, ElevatorController::State state);
	char getch();
};

#endif /* ELEVATOR_H_ */
